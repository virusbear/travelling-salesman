import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import processing.core.PApplet;
import processing.core.PVector;


public class Milestone {
	private PApplet p;
	private List<Milestone> mChildren;
	private Milestone mParent;
	private PVector mPosition;
	private float mRadius;
	
	public Milestone(PApplet p, PVector pos){
		this(p, pos, new ArrayList<Milestone>());
	}
	
	public Milestone(PApplet p, PVector position, List<Milestone> children){
		this(p, position, children, 0.0f);
	}
	
	public Milestone(PApplet p, PVector position, List<Milestone> children, float radius){
		this.p = p;
		mPosition = position;
		mChildren = children;
		mParent = null;
		mRadius = radius;
		
		mChildren.stream().forEach((child) -> child.setParent(this));
	}

	public void grow(){
		mRadius += 0.25f;
	}
	
	public float getRadius(){
		return mRadius;
	}

	public PVector getPosition(){
		return mPosition;
	}

	public boolean hasChildren(){
		return mChildren.size() != 0;
	}
	
	public List<Milestone> getChildren(){
		return mChildren;
	}
	
	public void setParent(Milestone parent){
		mParent = parent;
	}
	
	public Milestone getParent(){
		return mParent;
	}
	
	public List<Milestone> collide(List<Milestone> other){
		return other.stream().filter((milestone) -> mPosition.copy().sub(milestone.mPosition).mag() - (mRadius + milestone.mRadius) <= 0).collect(Collectors.toList());
	}
	
	public void render(){
		p.pushMatrix();
		p.pushStyle();
		
		p.translate(mPosition.x, mPosition.y);
		
		p.stroke(0xFF);
		p.strokeWeight(2);
		p.fill(p.color(255, 255, 255, 127));
		
		if(mParent == null){ //Render circle
			p.point(0, 0);
			p.ellipse(0, 0, mRadius * 2, mRadius * 2);
		} else { //render cross
			p.line(0, -5, 0, 5);
			p.line(-5, 0, 5, 0);
		}
		
		p.popMatrix();
		p.popStyle();
		
		mChildren.stream().forEach(Milestone::render);
	}

	public static Milestone of(PApplet p, List<Milestone> children) {
		PVector avgPosition = children.stream().map((child) -> child.mPosition).reduce(new PVector(0, 0), (avg, child) -> avg.add(child)).div(children.size());
		float maxRadius = (float) children.stream().mapToDouble((child) -> child.mRadius).max().getAsDouble();
		
		return new Milestone(p, avgPosition, children, maxRadius);
	}
}
