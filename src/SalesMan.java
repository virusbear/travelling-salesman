import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import processing.core.PApplet;
import processing.core.PVector;


public class SalesMan extends PApplet{
	
	private List<Milestone> milestones = new ArrayList<>();
	boolean collisionsDetected = false;
	
	public static void main(String[] args){
		PApplet.main("SalesMan");
	}
	
	public void settings(){
		size(800, 800, FX2D);
	}
	
	public void setup(){
		frameRate(60);
		IntStream.range(0, 40).forEach((idx) -> milestones.add(new Milestone(this, new PVector(random(width), random(height)))));
	}
	
	public void draw(){
		background(0);
		milestones.stream().forEach((milestone) -> {
			milestone.grow();
			milestone.render();
		});
		
		if(milestones.size() == 1){
			noLoop();
		}
		
		collisionsDetected = false;
		do{
			List<Milestone> copy = new ArrayList<>(milestones);
			for(Milestone milestone : copy){
				List<Milestone> collisions = milestone.collide(copy.stream().filter((m) -> m != milestone).collect(Collectors.toList()));
				
				if(collisions.size() != 0){
					collisions.add(milestone);
					milestones.removeAll(collisions);
					milestones.add(Milestone.of(this, collisions));
					collisionsDetected = true;
					break;
				}
				collisionsDetected = false;
			}
		} while(collisionsDetected);
		
		save(frameCount + ".png");
	}
}
